from datetime import datetime, timedelta
import sys
from django.core.management.base import BaseCommand, CommandError

from news.daryouz.parser.daryo_url import get_urls_by_date

class Command(BaseCommand):
    help = 'Daryo.uz'

    def add_arguments(self, parser):
        parser.add_argument('cmd', nargs='?', type=int)

    def handle(self, *args, **options):
        date_cmd = options["cmd"]

        if date_cmd == 1:
            date = datetime.today()
        elif date_cmd == 2:
            date = datetime.today() - timedelta(days=1)
        elif date_cmd == 3:
            date = datetime.today() - timedelta(days=65)
        elif date_cmd == 4:
            date = datetime.today() - timedelta(days=500)
        else:
            print("?")
            date = datetime.today() - timedelta(days=1)


        get_urls_by_date(date=date)
