from datetime import datetime
from django.db import models


class DaryoBaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class DaryoCategory(DaryoBaseModel):
    url = models.URLField(max_length=500, null=True, blank=True)
    name = models.CharField(max_length=200, unique=True)

    def __str__(self) -> str:
        return self.name


class DaryoPost(DaryoBaseModel):
    url = models.URLField(max_length=500, unique=True)

    title = models.CharField(max_length=500, null=True, blank=True)
    context = models.TextField(null=True, blank=True)

    category = models.ForeignKey(
        DaryoCategory, 
        on_delete=models.PROTECT,
        related_name="posts",
        null=True, 
        blank=True
    )

    published_at = models.DateTimeField(null=True, blank=True)
    

class DaryoPostViews(models.Model):
    post = models.ForeignKey(DaryoPost, on_delete=models.CASCADE, related_name="views_stats")
    views_count = models.IntegerField(default=0)
    checked_time = models.DateTimeField(default=datetime.now)

