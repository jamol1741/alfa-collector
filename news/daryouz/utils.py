from datetime import (
    datetime, 
    timedelta, 
    time
)

from urllib import parse

from news.daryouz.constants import UZ_MONTH


def unq(x):
    return parse.unquote(x)


def converter_date(p_dict: dict):
    now = datetime.now()
    p_time_str = p_dict['time']
    p_time = datetime.strptime(p_time_str, '%H:%M').time()

    if p_dict["full_date"]:
        result = datetime.strptime(
            f'{p_dict["full_date"]} {p_time_str}', "%d.%m.%Y %H:%M"
        )
    elif p_dict["this_year"]:
        result = datetime(
            year=now.year, 
            month=UZ_MONTH[p_dict["month"]], 
            day=int(p_dict["day"]), 
            hour=p_time.hour, 
            minute=p_time.minute
        )
    elif p_dict["day_name"]:
        is_today = True
        if p_dict["day_name"] == "Kecha":
            is_today = False
        
        yesterday = now - timedelta(days=1)

        result = datetime(
            year=now.year,
            month=now.month,
            day=now.day if is_today else yesterday.day,
            hour=p_time.hour, 
            minute=p_time.minute,
        )
    else:
        return None
            

    return result