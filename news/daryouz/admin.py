from django.contrib import admin

from news.daryouz.models import (
    DaryoPostViews,
    DaryoCategory,
    DaryoPost
)

@admin.register(DaryoPost)
class DaryoPostAdmin(admin.ModelAdmin):
    list_display = ("title", "url", "category", "context")


@admin.register(DaryoCategory)
class DaryoCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "url")


@admin.register(DaryoPostViews)
class DaryoPostViewsAdmin(admin.ModelAdmin):
    list_display = ("post", "views_count", "checked_time")