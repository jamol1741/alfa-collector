BASE_URL = "https://daryo.uz/"

REG_PUBLISH_TIME = r"(?P<date>"\
                        r"(?P<this_year>"\
                            r"(?P<day>\d{1,2}) ?"\
                            r"(?P<month>yanvar|fevral|mart|aprel|may|iyun|iyul|avgust|sentyabr|oktyabr|noyabr|dekabr))|"\
                        r"(?P<day_name>Bugun|Kecha)|"\
                        r"(?P<full_date>\d{1,2}.\d{2}.\d{4})), "\
                   r"(?P<time>\d{1,2}:\d{2})"

UZ_MONTH = {
    "yanvar": 1,
    "fevral": 2,
    "mart": 3,
    "aprel": 4,
    "may": 5,
    "iyun": 6,
    "iyul": 7,
    "avgust": 8,
    "sentyabr": 9,
    "oktyabr": 10,
    "noyabr": 11,
    "dekabr": 12,
}

ARCHIVE_URL = 'https://daryo.uz/content/archive/'

GET_URLS_HEADER = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0',
        'Accept': '*/*',
        'Accept-Language': 'en-US,en;q=0.5',
        'X-Requested-With': 'XMLHttpRequest',
        'DNT': '1',
        'Connection': 'keep-alive',
        'Referer': 'https://daryo.uz/archive/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
    }
