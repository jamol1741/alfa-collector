import re
import sys
import logging
import requests
from datetime import time
from urllib.parse import urljoin
from datetime import datetime, timedelta

from bs4 import BeautifulSoup
from django.utils.timezone import make_aware

from news.daryouz.constants import ARCHIVE_URL, BASE_URL, GET_URLS_HEADER, REG_PUBLISH_TIME
from news.daryouz.utils import converter_date, unq
from news.daryouz.models import DaryoCategory, DaryoPost


logger = logging.getLogger("daryouz")
logger.setLevel("INFO")

def get_urls_by_date(date: datetime):

    logger.info(f"date: {date}")
    page = 1
    while True:
        params = {
            'date': date.strftime("%d-%m-%Y"),
            'page': str(page),
            'per-page': '4',
        }

        response = requests.get(
            ARCHIVE_URL, 
            params=params, 
            headers=GET_URLS_HEADER
        )
        content = response.content
        soup = BeautifulSoup(content, "html.parser")

        page += 1

        # collect links
        posts = soup.select("div.mini__article-item")

        for post in posts:
            category_box = post.select_one("a.category__title")

            if not category_box:
                category = None
                category_url = None
            else:
                category = category_box.text.strip()
                category_url = category_box["href"]
                category_url = urljoin(BASE_URL, category_url)

            title_box = post.select_one("a.mini__article-link")

            title = title_box.text.strip()

            if not title: # Deleted news
                continue

            u_url = unq(unq(title_box["href"]))
            url = urljoin(BASE_URL, u_url)

            published_at_str = post.select_one("time.meta-date").text.strip()

            p_result = re.search(REG_PUBLISH_TIME, published_at_str)

            p_dict = p_result.groupdict()

            published_at = make_aware(converter_date(p_dict))

            views_count = post.select_one("span.meta-views-count").text.strip()

            views_count = int(views_count)

            # save objects
            if not category:
                daryo_category = None
            else:
                daryo_category = DaryoCategory.objects.get_or_create(
                    name=category,
                    defaults={
                        "url": category_url
                    }
                )[0]

            daryo_post = DaryoPost.objects.update_or_create(
                url=url,
                defaults={
                    "category": daryo_category,
                    "title": title,
                    "published_at": published_at
                }
            )[0]
        
        _next_page = soup.select_one("div.mini__article-btn")

        if not _next_page:
            break


